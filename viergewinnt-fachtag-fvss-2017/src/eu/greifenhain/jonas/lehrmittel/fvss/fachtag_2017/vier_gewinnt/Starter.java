package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt;

/**
 *
 * @author Jonas Greifenhain
 * 
 * Diese Klasse startet das Spiel.
 * 
 * Sie muss/sollte nicht verändert werden.
 */
public class Starter
{

    /**
     * Diese Funktion startet das Spiel.
     * 
     * @param args Startparameter
     */
    public static void main(String[] args)
    {
        VierGewinnt vierGewinnt = new VierGewinnt();
    }
}
