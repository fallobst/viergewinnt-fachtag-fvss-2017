package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt;

import eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.gui.MainGui;
import eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.material.Spielfeld;
import eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.tools.Netzverbindung;

/**
 *
 * @author Jonas Greifenhain
 * 
 * Diese Klasse verbindet die einzelnen Programmbestandteile.
 */
public class VierGewinnt
{
    /**
     * true = Spieler
     * false = Gegenspieler
     */
    private boolean _aktiverSpieler;
    
    /**
     * Die Oberfläche des Spiels
     */
    private MainGui _gui;
    
    /**
     * Das Spielfeld des Spiels
     */
    private Spielfeld _spielfeld;
    
    /**
     * Die Netzwerkschnittstelle des Spiels
     */
    private Netzverbindung _netzverbindung;
    
    public VierGewinnt()
    {
        
    }
}
