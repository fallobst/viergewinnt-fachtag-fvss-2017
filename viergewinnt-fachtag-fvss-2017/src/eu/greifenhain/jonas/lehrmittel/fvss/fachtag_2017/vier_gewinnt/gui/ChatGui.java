package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.gui;

import java.awt.event.ActionListener;
import javax.swing.JPanel;

/**
 * 
 * @author Jonas Greifenhain
 */
public class ChatGui extends JPanel
{
    public ChatGui()
    {
        //JPanel initialisieren
        super();
    }
    
    /**
     * Diese Nachricht gibt eine neu geschriebene ChatNachricht des Spielers
     * zurück.
     * 
     * @return Die Nachricht des Spielers
     */
    public String getChatText()
    {
        return null;
    }
    
    /**
     * Diese Funktion fügt zu dem Chatverlauf eine Nachricht hinzu, die der
     * Gegner geschrieben hat.
     * 
     * @param text Die Nachricht vom Gegner
     */
    public void addChatText(String text)
    {
        
    }
    
    /**
     * Der hinzuzufügende ActionListener wird anschlagen, wenn der Benutzer eine
     * Chat-Nachricht senden möchte.
     * 
     * @param al Der hinzuzufügende ActionListener
     */
    public void addChatActionListener(ActionListener al)
    {
        
    }
}
