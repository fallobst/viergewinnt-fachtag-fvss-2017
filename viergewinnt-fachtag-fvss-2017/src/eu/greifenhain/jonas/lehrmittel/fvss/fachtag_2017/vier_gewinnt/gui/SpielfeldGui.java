package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.gui;

import eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.material.Spielfeld;
import java.awt.event.ActionListener;
import javax.swing.JPanel;

/**
 *
 * @author Jonas Greifenhain
 */
public class SpielfeldGui extends JPanel
{
    public SpielfeldGui()
    {
        //JPanel initialisieren
        super();
    }
    
    /**
     * Diese Funktion stellt den aktuellen Zustand von "spielfeld" un der GUI
     * dar.
     * 
     * @param spielfeld Das darzustellende Spielfeld
     */
    public void zeigeSpielfeld(Spielfeld spielfeld)
    {
        
    }
    
    /**
     * Diese Funktion gibt die zuletzt vom Spieler ausgewählte Spalte zurück.
     *   0 ist dabei ganz links.
     * 
     * @return die zuletzt vom Spieler ausgewählte Spalte.
     */
    public int getGewaehlteSpalte()
    {
        return -1;
    }
    
    /**
     * Der hinzuzufügende ActionListener schlägt an, wenn der Spieler eine
     * Spalte ausgewählt hat.
     * 
     * @param al Der hinzuzufügende ActionListener
     */
    public void addSpaltenwahlActionListener(ActionListener al)
    {
        
    }
}
