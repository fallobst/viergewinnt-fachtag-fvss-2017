package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.gui;

import java.awt.event.ActionListener;
import javax.swing.JFrame;

/**
 *
 * @author Jonas Greifenhain
 */
public class StartGui extends JFrame
{
    public StartGui()
    {
        //Initialisieren der übergeordneten Klasse (JFrame)
        super("Fenstertitel");
    }
    
    /**
     * Diese Funktiion gibt zurück, ob das Spiel als Client oder Server arbeiten
     * soll.
     * 
     * false = Verbindung aufbauen
     * true  = Auf Verbindung warten (Server-Modus)
     * 
     * @return Der Verbindungsmodus
     */
    public boolean getConnectionMode()
    {
        return false;
    }
    
    /**
     * Diese Funktion gibt die Adresse des Servers zurück, wenn sich das Spiel
     * mit einem anderen Spieler verbinden soll.
     * (getConnectionMode() == false) -> Verbindung aufbauen
     * 
     * @return 
     */
    public String getHost()
    {
        return null;
    }
    
    /**
     * Diese Funktion gibt den angegebenen Port zurück
     * 
     * @return Der angegebene Port
     */
    public int getPort()
    {
        return -1;
    }
    
    /**
     * Der hinzuzufügende ActionListener wird anschlagen, wenn der Benutzer eine
     * Verbindung aufbauen will.
     * 
     * @param al Der hinzuzufügende ActionListener
     */
    public void addVerbindenActionListener(ActionListener al)
    {
        
    }
}
