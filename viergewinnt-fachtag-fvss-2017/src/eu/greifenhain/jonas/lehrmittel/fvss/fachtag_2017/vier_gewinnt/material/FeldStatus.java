package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.material;

/**
 *
 * @author Jonas Greifenhain
 * 
 * Diese Klasse beinhaltet Konstanten, die den Zustand eines Feldes darstellen.
 * 
 * Diese Klasse ist bereits fertig.
 */
public class FeldStatus
{
    /**
     * Das Feld ist unbelegt
     */
    public static final int UNBELEGT = 0;
    
    /**
     * Der Spieler hat das Feld belegt.
     */
    public static final int SPIELER = 1;
    
    /**
     * Der Gegenspieler hat das Spiel belegt.
     */
    public static final int GEGENSPIELER = 2;
}
