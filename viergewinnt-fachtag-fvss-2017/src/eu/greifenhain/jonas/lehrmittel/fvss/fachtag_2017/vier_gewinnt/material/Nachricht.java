package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.material;

/**
 *
 * @author Jonas Greifenhain
 * 
 * Ein Objekt dieser Klasse stellt eine Nachricht dar.
 * 
 * Diese Klasse ist bereits fertig
 */
public class Nachricht
{
    public static final int POSITION = 0;
    public static final int CHAT = 1;
    
    /**
     * Diese Variable sagt aus, was die Nachricht aussagen soll.
     */
    private final int _messageType;
    
    /**
     * Diese Variable speichert die eigentliche Nachricht.
     * Es kann ein beliebiges Objekt gespeichert werden.
     * "_messageType" sagt aus, um welchen Object-Typ es sich handelt.
     * Steht das fest wird wie folgt vorgegangen (in diesem Fall handelt es sich
     * um einen String)
     *   String s = (String)_parameter;
     * Statt "_parameter" wird von au�en "getParameter" aufgerufen.
     */
    private final Object _parameter;
    
    /**
     *  
     * @param messageType Der Typ der Nachricht
     * @param parameter Die eigentlich Nachricht (siehe Kommentar oben)
     */
    public Nachricht(int messageType, Object parameter)
    {
        _messageType = messageType;
        _parameter = parameter;
    }
    
    /**
     * Diese Funktion gibt den Typ der Nachricht zur�ck.
     * 
     * @return Typ der Nachricht.
     */
    public int getType()
    {
        return _messageType;
    }
    
    /**
     * Diese Funktion gibt die eigentliche Nachricht zur�ck.
     * 
     * @return Die eigentliche Nachricht (Verwendung: siehe Kommentar oben)
     */
    public Object getParameter()
    {
        return _parameter;
    }
}