package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.material;

/**
 *
 * @author Jonas Greifenhain
 * 
 * Ein Objekt dieser Klasse stellt eine Position auf dem Spielfeld dar.
 * Der X-Wert geht von 0 bis einschließlich 6
 * Der Y-Wert geht von 0 bis einschließlich 4
 * 
 * (X=0; Y=0) entspricht oben links
 * (X=0; Y=4) entspricht oben rechts
 * (X=6; Y=0) entspricht unten links
 * (X=6; Y=4) entspricht unten rechts
 * 
 * Diese Klasse ist bereits fertig.
 */
public class Position
{
    private final int _posX;
    private final int _posY;
    
    public Position(int posX, int posY)
    {
        _posX = posX;
        _posY = posY;
    }
    
    public int getX()
    {
        return _posX;
    }
    
    public int getY()
    {
        return _posY;
    }
}
