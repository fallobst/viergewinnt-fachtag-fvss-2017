package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.material;

import java.util.List;

/**
 *
 * @author Jonas Greifenhain
 * 
 * Das Spielfeld sollte 7 Reihen und 5 Zeilen haben.
 */
public class Spielfeld
{
    /**
     * Mit dieser Funktion wird der Zustand des oberen Feldes in einer Spalte
     * verändert.
     * Die Zustände werden den Konstanten von "FeldStatus" entnommen.
     * Diese Funktion darf nur benutzt werden, wenn
     * getViererReihe() == null
     * 
     * Beispiel:
     * setPosition(position, FeldStatus.SPIELER)
     * 
     * @param position Die Position des zu verändernden Feld.
     * @param state Der Zustand des Feldes.
     */
    public void setSpalte(int spalte, int state)
    {
        
    }
    
    /**
     * Diese Funktion gibt den Zustand einer Position zurück. Der Zustand ist
     * eine Konstante aus "FeldStatus".
     * 
     * @param position Die abzufragende Position
     * @return Der Zustand an "position"
     */
    public int getWert(Position position)
    {
        return 0;
    }
    
    /**
     * Wenn ein Spieler gewonnen hat, gibt diese Funktion eine Liste mit
     * den Positionen zurück, die eine Vierer-Reihe ergeben.
     * Hat kein Spieler gewonnen, gibt die Funktion null zurück.
     * 
     * @return Die Sieg-Vierer-Reihe, wenn möglich
     */
    public List<Position> getViererReihe()
    {
        return null;
    }
}
