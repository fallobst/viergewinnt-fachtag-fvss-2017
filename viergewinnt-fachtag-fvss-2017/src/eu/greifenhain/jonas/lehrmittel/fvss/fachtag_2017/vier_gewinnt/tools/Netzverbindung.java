package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.tools;

import eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.material.Nachricht;
import java.util.List;

/**
 *
 * @author Jonas Greifenhain
 */
public class Netzverbindung
{
    private final Parser _parser;
    
    /**
     * Mit diesem Constructor wird verbindet sich das erzeugte Objekt mit einem
     * Spielserver.
     * 
     * @param host Die Adresse des Spielservers
     * @param port Der Port des Spielservers
     */
    public Netzverbindung(String host, int port)
    {
        
        _parser = new Parser();
    }
    
    /**
     * Mit diesem Constructor wird das erzeugte Object zum Spielserver.
     * Der Construktor wartet auf eine Verbindung.
     * 
     * @param port Der Port, auf dem der Server gestartet werden soll
     */
    public Netzverbindung(int port)
    {
        
        _parser = new Parser();
    }
    
    /**
     * Diese Funktion sendet eine Nachricht
     * 
     * @param nachricht Die Nachricht, die gesendet werden soll
     */
    public void sendeNachricht(Nachricht nachricht)
    {
        
    }
    
    /**
     * Diese Funktion gibt eine Liste mit alles Nachrichten, die seit dem
     * letzten Aufrufen empfangen wurden, zurück.
     * 
     * @return Die Liste der Nachrichten
     */
    public List<Nachricht> getLetzteNachrichten()
    {
        return null;
    }
}
