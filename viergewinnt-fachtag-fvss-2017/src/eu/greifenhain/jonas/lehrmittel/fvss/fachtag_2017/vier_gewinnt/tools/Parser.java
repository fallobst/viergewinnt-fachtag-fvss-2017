package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.tools;

import eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.material.Nachricht;

/**
 *
 * @author Jonas Greifenhain
 */
public class Parser
{
    /**
     * Diese Funktion wertet einen Text vom Gegenspieler aus, sodass er
     * weiterverarbeitet werden kann.
     * 
     * @param nachricht Der Text, der vom Gegner empfangen wurde
     * @return Die ausgewertete Nachricht
     */
    public Nachricht parseNachricht(String nachricht)
    {
        return null;
    }
    
    /**
     * Diese Funktion wandelt eine Nachricht einen Text um, sodass sie über das
     * Netzwerk gesendet werden kann.
     */
    public String parseNachricht(Nachricht nachricht)
    {
        return null;
    }
}
